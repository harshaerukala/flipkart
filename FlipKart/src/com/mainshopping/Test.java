package com.mainshopping;

// importing local dependencies
import com.onlineshopping.Product;


 

import com.onlineshopping.ProductCollection;

import java.util.HashSet;

import com.onlineshopping.Address;
import com.onlineshopping.AddressCollection;
import com.onlineshopping.Customer;
import com.onlineshopping.Order;
import com.onlineshopping.Payment;
import com.onlineshoppingbuy.Rating;

/**
 * Test class. Contains main method to test various features during development.
 * 
 * @author harsha1
 *
 */
public class Test {
	/**
	 * Main method to test various features
	 */
	public static void main(String[] args) throws Exception {
		
		Test testerObject = new Test();
		
		testerObject.testProductCreation();

		 testerObject.testRatingCreation();
		
		 testerObject.testAddressCreation();
        testerObject.testCustomerCreation();
	}
	
	/**
	 * Instance method to test Product object creation
	 */
	private void testProductCreation() {

		ProductCollection pc = new ProductCollection();
		pc.addProduct(" Shoes 1", 6000f, "super shoes, orange color");
		pc.addProduct("Shoes 2", 8099f, "nice shoes, yellow color");
		pc.addProduct("Shoes 3", 9000f, "fantastic  shoes, red color");
		
		 pc.printAllProducts();
		 
		
		HashSet<Product> results = pc.returnSearchResultsProducts("blue");
		// System.out.println(results.size());
		 //pc.printTheseProducts(results);
		//Product product48496 = pc.getProductById("48496");
		Product pro = pc.getproductByDescription("red color");
		//Product.addRating(4, "This is awesome product", "12356");
		
		
	}
	
	/**
	 * Instance method to test Rating object creation
	 */
	private void testRatingCreation() {
		
		try {
			// testing rating creation
			Rating myrating = new Rating("500", (byte) 2, "good ");
			System.out.println(myrating);
			Rating mysecondrating = new Rating("300", (byte) 3, "very good");
			System.out.println(mysecondrating);
			Rating mythirdrating = new Rating("490", (byte) 5, " notgood");
			System.out.println(mythirdrating);
			
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
		}
	}

	/**
	 * Instance method to test Address object creation
	 */
	private void testAddressCreation() {
try {
		AddressCollection ac = new AddressCollection();
			ac.addAddress("1-11","warangal","parkal","hyderabad","445674","vijayawada","India");
			ac.addAddress("2-11","orr circle","hyderabad","gachibowli","500004", "Hyderabad","India");
			HashSet<Address> results = ac.returnSearchResultsProducts("456888");
			System.out.println(results.size());
			ac.printTheseProducts(results);
} catch(Exception e) {
	System.out.println(e.getLocalizedMessage());
}
				}
	   
 private void testCustomerCreation() {
	 try{
		 Customer cust = new Customer("1234567","Erukala","harshavardhinierukala67@gmail.com","harsha987","70-13462028","Female");
		 System.out.println(cust);
	 } catch(Exception e) {
		 System.out.println(e.getLocalizedMessage());
	 } 
 }



}

