package com.onlineshopping;
/**this is implementation of address by using the constructor methods 
 * 
 * @author harsha1
 *
 */
public class Address {
	String house_no;   //representation of houseno
	String Street;     //representation of street
	String city;       //representation of city
	String Distict;    //representation of distict
	String state;      //representation of state
	String country;    //representation of country
	String pincode;    //representation of pincode
	//default constructor
//	public Address()
//	{
//		System.out.println("this is default constructor");
//	}
	//basic constructor
	public Address(String house_no,String Street,String city,String Distict,String state,String country,String pincode)
	{
		this.house_no = house_no;
		this.Street   = Street;
		this.city     = city;
		this.Distict  = Distict;
		this.state    = state;
		this.country  = country;
		this.pincode  = pincode;
	}
	//toString method
	@Override
	public String toString() {
		String stringaddr ="this is delivery address";
		stringaddr+="house_no :     "+this.house_no+"\n";
		stringaddr+="Street   :     "+this.Street+"\n";
		stringaddr+="city     :     "+this.city+"\n";
		stringaddr+="Distic   :     "+this.Distict+"\n";
		stringaddr+="state    :     "+this.state+"\n";
		stringaddr+="country  :     "+this.country+"\n";
		stringaddr+="pincode  :     "+this.pincode+"\n";
		return stringaddr;
	}
	public String getHouse_no() {
		return house_no;
	}
	public String getStreet() {
		return Street;
	}
	public String getCity() {
		return city;
	}
	public String getDistict() {
		return Distict;
	}
	public String getState() {
		return state;
	}
	public String getCountry() {
		return country;
	}
	public String getPincode() {
		return pincode;
	}
	
}