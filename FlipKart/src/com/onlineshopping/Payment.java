package com.onlineshopping;
/**
 * 
 * @author vchukka
 *
 */
public class Payment {
	String paymentId;
	String status;
	String modeOfPayment;
	
	public Payment(String paymentId,String status,String modeOfPayment) {
		
		this.paymentId     = paymentId;
		this.status        = status;
		this.modeOfPayment = modeOfPayment;
	}
	@Override
	public String toString() {
		String payment = "";
		payment += "paymentId     :    "+this.paymentId+"\n";
		payment +="status         :    "+this.status+"\n";
		payment +="modeOfPayment  :    "+this.modeOfPayment+"\n";
		return payment;
	} 

}
