package com.onlineshopping;

import java.util.ArrayList;

public class Customer {
	String c_id;
	String name;
	String email;
	String password;
	String phone_no;
	String gender;
	ArrayList<Address> address;
	
	public Customer()
	{
		System.out.println("this is default of address constructor");
	}
	public Customer(String c_id,String name,String email,String password,String phone_no,String gender){
		this.c_id = c_id;
		this.name = name;
		this.email = email;
		this.password = password;
		this.gender = gender;
	}
	@Override
	public String toString() {
		String custreturn = "";
		custreturn+="c_id     :   "+this.c_id+"\n";
		custreturn+="name     :   "+this.name+"\n";
		custreturn+="email    :   "+this.email+"\n";
		custreturn+="password :   "+this.password+"\n";
		custreturn+="gender   :   "+this.gender+"\n";
		
		return custreturn;
	}

}
