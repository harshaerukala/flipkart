package com.onlineshopping;

// Java dependencies
import java.util.HashSet;
 

import java.util.Iterator;

//import com.onlineshoppingbuy.Rating;

/**
 * This is a wrapper class to contain all the products in the 
 * FlipKart application.
 * @author harsha1
 */
public class ProductCollection {
	
	private static HashSet<Product> listOfAllProducts;
	
	/** Constructor */
	public ProductCollection() {
		listOfAllProducts = new HashSet<Product>();
	}
	
	/**
	 * This is a helper method, to create a Product object.
	 * @param name
	 * @param price
	 * @param description
	 */
	public boolean addProduct(String name, float price, String description) {

		try {
			Product newProduct = new Product(name, price, description);
			listOfAllProducts.add(newProduct);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	
	/**
	 * This is a helper method, to print all products in collection 
	 * in human readable format.
	 */
	public void printAllProducts() {
		for (Iterator<Product> iter = listOfAllProducts.iterator(); iter.hasNext();) {
			Product currentProduct = (Product) iter.next();
			System.out.println(currentProduct);
		}
	}
	
	/**
	 * Helper method to print the set of products specified as parameter
	 * @param productsSet a HashSet<Product> to be printed in human readable format
	 */
	public void printTheseProducts(HashSet<Product> productsSet) {
		
		for (Product currProduct : productsSet) {
			System.out.println(currProduct);
		}
	}
	
	/**
	 * Helper method to accept search string to search for products matching string.
	 * @param searchString
	 * @return A Set of products matching the searchString
	 */
	public HashSet<Product> returnSearchResultsProducts(String searchString) {
		HashSet<Product> returnSet = new HashSet<Product>();
		
		for (Product currProduct : listOfAllProducts) {
			System.out.println(currProduct.getName());
			System.out.println(currProduct.getDescription());

			System.out.println(searchString);
			
			if ((currProduct.getName().contains(searchString) ||
					currProduct.getDescription().contains(searchString))) {
				returnSet.add(currProduct);
				System.out.println("Matched!");
			} else {
				System.out.println("Mismatch!");
			}
		}	
		return returnSet;
	}

	/**
	 * Helper method to find a product by its id
	 * @param p_id	
	 * @return the PRoduct object if found, or null object if not found
	 */
	public Product getProductById(String searchID) {
		
		for (Product currProduct : listOfAllProducts) {
			if (currProduct.getID().matches(searchID)) {
				return currProduct;
			} 
		}
		return null;
	}
	public Product getproductByDescription(String searchDescription) {
		for (Product currproduct : listOfAllProducts) {
			if(currproduct.getDescription().matches(searchDescription)) {
			System.out.println("this is good");
			return currproduct;
			}
			
		}
	return null;
	}
	
}
