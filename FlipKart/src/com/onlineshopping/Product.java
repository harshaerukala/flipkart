package com.onlineshopping;

// Java dependencies
import java.util.ArrayList;
 
 
import java.lang.Exception;
// local dependencies
import com.onlineshoppingbuy.Rating;

/**
 * Class representing a Product in FlipKart application.
 * @author harsha1
 */
public class Product {

	private static final Product[] listOfAllProducts = null;

	private static int id_counter = 48494;
	
    private String   p_id;       			// representation of product id
	private String   name;           		// representation of name
	private float    price;          		// representation of price  
	private String   description;    		// representation of description 
	
	private ArrayList<Rating> ratings;   	// rating array contains Rating objects

	/**
	 * parameterized constructor
	 * 
	 * @param name			name of the product, 2 to 40 characters allowed
	 * @param price			price of the product, has to be between 1 and 1000000 (10 lakhs)
	 * @param description	description of the product, 2 to 260 characters allowed
	 * @throws Exception	throws exception when parameters are wrong
	 */
	public Product(String name,float price,String description) throws Exception {
		
		if(name.trim().length() < 2 || name.trim().length() > 40) {
			// validation of name failure
			throw new Exception("Name is too short.");
		}
		else if(description.trim().length() < 2 || description.trim().length() > 260) {
			// validation of description failure
			throw new Exception("Description is too short");
		}
		else if(price < 1 || price > 1000000) {
			// validation of price failure
			throw new Exception("price is too low or negative");
		} else {
			this.name 			= name;
			this.price 			= price;
			this.description 	= description;
			this.p_id 			= ++id_counter + "";
			this.ratings 		= new ArrayList<Rating>();
		}
	}
	
	/**
	 * Overriding the toString method of Product object so that
	 * System.out.println is human readable.
	 */
	@Override
	public String toString() {
		
		String stringToReturn = "This is a product object details below:\n";
		stringToReturn += "p_id:         " + this.p_id+ "\n";
		stringToReturn += "name:         " + this.name+"\n";
		stringToReturn += "description:  " + this.description+"\n";
		stringToReturn += "price:        " + this.price+"\n";
		stringToReturn += "--------------------------------------------";

		return stringToReturn;
	}

	/**
	 * Getter for the name property
	 * @return name of the product as String
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Getter for the description property
	 * @return description of the product as String
	 */
	public String getDescription() {
		return this.description;
	}
	
	/**
	 * Getter for the product id property
	 * @return description of the product as String
	 */
	public String getID() {
		return this.p_id;
	}
}
	
	

	



	
	
		




