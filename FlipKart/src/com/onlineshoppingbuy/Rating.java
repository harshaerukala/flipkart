package com.onlineshoppingbuy;
/**
 * this is the implementation of the Rating by using the different
 * data types and different variables 
 * @author harsha1
 *
 */

public class Rating {
	String cust_id;       //this is representation of customer id
	byte rating_number;   //this is represetation of review text
	String  rating_text;  //this is representation of number rating
	//default constructor
	public Rating()
	{
		System.out.println("this is default rating");
	}
	//this is basic constructor
	public Rating(String cust_id,byte rating_number,String rating_text ) throws Exception
	{
		if(rating_text.trim().length() > 20) //throws Exception 
		{ 
			throw new Exception("length is tooo long");
		}
		else if (rating_number > 5  ) { //throws Exception
		throw new Exception("rating is too high");	
		}
		
		this.cust_id       =  cust_id;
		this.rating_number =  rating_number;
		this.rating_text   =  rating_text;
	 }
	//toString method
	@Override
	public String toString() {
		String stringreturn ="this is the rating of the products\n";
		 stringreturn +="cust_id          :   "+this.cust_id+"\n";
		 stringreturn +="rating_number    :   "+this.rating_number+"\n";
		 stringreturn +="rating_text      :   "+this.rating_text+"\n";
		 stringreturn +="$%----------------------------------------------------%$";
		 return stringreturn;	
	}
	public String getCust_id() {
		return cust_id;
	}
	public byte getRating_number() {
		return rating_number;
	}
	public String getRating_text() {
		return rating_text;
	}
}
